<?php

require_once "Support.php";

class Dvd extends Support{
    
    private $duree;
    private $jaquette;
    private $estCollector;
    private $descriptionFeature;
    
    public function __toString()
    {
        return $this->duree . " minutes";
    }

    public function __construct()
    {
        // initie un tableau vide pour forcer le type de cet attribut
        $this->descriptionFeature = [];
    }

    public function AddDescriptionFeature($df){
        // $this->descriptionFeature[] = $df;
        array_push($this->descriptionFeature,$df);
    }

    /*
     * Accesseur/mutateur
     * principe d'encapsulation
     */
    public function setDuree($duree){

        $this->duree = $duree;
    }

    public function getDuree(){
        return $this->duree;
    }

    public function play(){}

    public function stop(){}

    /**
     * Get the value of jaquette
     */ 
    public function getJaquette()
    {
        return $this->jaquette;
    }

    /**
     * Set the value of jaquette
     *
     * @return  self
     */ 
    public function setJaquette($jaquette)
    {
        $this->jaquette = $jaquette;

        return $this;
    }

    /**
     * Get the value of estCollector
     */ 
    public function getEstCollector()
    {
        return $this->estCollector;
    }

    /**
     * Set the value of estCollector
     *
     * @return  self
     */ 
    public function setEstCollector($estCollector)
    {
        $this->estCollector = $estCollector;

        return $this;
    }

    /**
     * Get the value of descriptionFeature
     */ 
    public function getDescriptionFeature()
    {
        return $this->descriptionFeature;
    }

    /**
     * Set the value of descriptionFeature
     *
     * @return  self
     */ 
    public function setDescriptionFeature($descriptionFeature)
    {
        $this->descriptionFeature = $descriptionFeature;

        return $this;
    }


}